\section{Implementation}\label{sec:implementation}
\ac{ucsp} implementation has been split into three main software components making it extensible, customizable and portable.
This structure has been designed to have self-contained codebases and to maximize portability and performance in target environments including \ac{iot}, mobile and cloud.
The three components are libalfa, ucon-c and ucon-service as described below.

\texttt{libalfa} is a library that incorporates the features of the \ac{alfa} policy language and encompasses \ac{pdp} functionalities (e.g., policy parsing and evaluation) and part of the \ac{pap} functionalities.
It has been entirely developed in C++ with no external dependencies, making it deployable on many different devices and with high performance.
libalfa is a self-contained module for \ac{abac}, which enables its use for scenarios where simple access control is needed without session support or external attributes.
libalfa also supports the following two \ac{xacml} profiles: 
\begin{enumerate}
    \item \ac{mdp}: used in use-cases where access to multiple resources can be requested within a single request.
    \item \ac{adp}: used to define a root of trust for policies creating a chain of authority to track responsibility and limit attack surface.
\end{enumerate}

\texttt{ucon-c} is a library that implements \ac{ucon}+ novelties such as session-based monitoring and policy re-evaluation according to the architecture described in Section~\ref{sec:architecture}.
It has also been entirely developed in C++ in order to allow an easy integration with libalfa, maximize performance, and enable its deployment on different and low-end devices.
ucon-c structure mirrors the block diagram in Figure~\ref{fig:arch}.
Programmable components have been implemented as abstract classes to guide and facilitate their implementation for the requirements of domain-specific use-cases. 
For instance, \acp{pip} are context-dependent and their behavior varies with the \acp{ar} they are connected to, but some of their functionalities are common and do not need to be re-implemented such as caching latest attribute values or enriching requests. 
ucon-c is \emph{language agnostic} making it adaptable to different policy-languages.
Therefore, libalfa can be substituted with other libraries that implement other policy languages such as \ac{opa} working with Rego or \ac{xacml} working with Balana/AuthzForce.

\texttt{ucon-service} is a service that exposes an \ac{api} that enables the use of \ac{ucsp} in cloud environments.
ucon-service has been developed using Golang due to scalability, performance and resilience requirements.
ucon-service exposes a gRPC \ac{api} that allows managing \acp{pip} and policies as well as initiating usage sessions by external \acp{pep}.
The service specifically exposes the following gRPC endpoints:
\begin{itemize}
    \item \texttt{Usage} endpoint invoked to start a usage session. It returns a sequence of responses corresponding to the three phases of \ac{ucon}+ sessions. The endpoint may also respond with an updated usage decision such as revoke, whenever a change occurs and policies are re-evaluated.
    \item \texttt{\ac{pap}} endpoint used to create, delete and retrieve policies. 
    \item \texttt{\ac{pip}} endpoint used to subscribe, unsubscribe and update attributes. 
\end{itemize}

\subsection{ADP Reduction Algorithm}\label{sec:reduction}
According to the \ac{adp}, a Usage Policy evaluation result will not be enforced unless evaluation of Administrative Policies result into a permit.
Thus, a Usage Policy can be \emph{Admissible} if its enforcement is authorized by all upper layer policies, or \emph{NotAdmissible} if one of the Administrative Policies in the hierarchy cannot be considered.
This is expressed in the evaluation outcomes truth table illustrated in Table \ref{tab:evaluation outcomes}, 
which shows that whenever Administrative Policies are admissible, a final result is obtained.
On the other hand, if one of the Administrative Policies is NotAdmissible, then another policy needs to be found.
Indeed, in some cases, multiple policies may be Admissible and have a Permit/Deny result.

A policy reduction algorithm is used to manage inconsistencies and to guide editing of administrative policies in order to avoid a situation where all evaluations end up in a NotAdmissible state. 
To achieve this, once an administrative policy is added, compliance with the above layers can be checked and suggestions for improvements may be provided.
Policy reduction is a process by which authority of a policy associated with an issuer is verified.
A reduction tree is built on the basis of the issuer ID of each policy in a policyset.
When an access/usage policy is evaluated, the reduction graph is searched for a trusted administrative policy, based on which the combined evaluation effect will be taken for consideration by the \ac{pdp}.
Listing \ref{lst:evaluationReduction} describes the reduction algorithm given a policy set and a request.
The policies are combined as usual with the defined combining algorithms.
\begin{lstlisting}[float=!htb, caption={Evaluation and Reduction process},label={lst:evaluationReduction}, escapeinside={(*}{*)}]
<given a policy set PS and a request R>
evaluate R against PS
find applicable policies Papp
for each applicable policy Pa in Papp
    if deny
        continue
    if PolicyIssuer is absent 
        then combine
    else 
        [selection step] : select administrative policy Padmin for Pa
            for each Pi in Padmin
                create administrative request Ra using R and Pi
                evaluate Ra against Pi
                if deny
                    no edge
                else
                    add edge
                    if !policyIssuer 
                        potential path is found
                    else 
                        go to selection step with Pi as Pa

combine edge results with PS combining algorithm
\end{lstlisting}


\subsection{Performance}
%According to \ac{opa}~\cite{opa} (put reference here) an acceptable access control system for high performance environment should be able to run in less than 1ms, our evaluator in certain scenarios has been able to perform a thorough policy evaluation respecting that requirement. Moreover, even when policies where more complex, the system has been able to perform policy evaluation in 6ms.
\ac{ucsp} has been designed to work on resource-limited devices such as \ac{iot} as well as cloud and high-performance environments.
We evaluated the performance of \ac{ucsp} in the following different circumstances:
\begin{itemize}
    \item \emph{Cold start}: No attributes are installed in the system. We measured both the end to end time to obtain an evaluation and re-evaluation time, considering the communication penalty and assuming all missing attributes come in the same time.
    \item \emph{Standard run}: All attributes are already installed.
    \item \emph{Re-evaluations}: Attributes are supplied one by one at different times in order to evaluate how much time the system takes to perform re-evaluations if attributes are not all immediately supplied.
\end{itemize}
We also evaluated the performance of the \emph{\ac{pdp}} to show the time it takes to perform a policy parsing and evaluation compared to Balana\footnote{\url{https://github.com/wso2/balana}} \ac{xacml} evaluator as shown in Figure~\ref{fig:perf} and Table~\ref{tab:policylength}.
We only measured the performance of \ac{abac} part of \ac{ucsp} \ac{pdp} in order to be coherent in our performance evaluation as Balana does not support \ac{ucon}.
The results show that our C++ implementation is more than 40 times faster than Balana. 

\begin{figure}[!htb]
    \centerline{\includegraphics[width=0.75\linewidth]{figures/barchart_pdp_eval_parse.png}}
    \caption{Performance of \ac{ucsp} \ac{alfa} Evaluator vs Balana XACML Evaluator}
    \label{fig:perf}
\end{figure}

\begin{table}[!htbp]
    \centering
    \caption {Policy length comparison (in KB)} \label{tab:policylength}
    \resizebox{0.70\linewidth}{!}{
    \begin{tabular}{|c|c|c|}
            \hline
            % \makecell{\textbf{Number of} \\ \textbf{attributes}} & \textbf{\ac{alfa}} & \textbf{XACML} \\
            \textbf{Number of attributes} & \textbf{\ac{alfa}} & \textbf{XACML}\\
            \hline
            5 & 1.1 & 8.2 \\
            \hline
            10 & 1.8 & 16 \\
            \hline
            15 & 2.6 & 24 \\
            \hline
            20 & 3.4 & 32 \\
            \hline
        \end{tabular}
    }
\end{table} 