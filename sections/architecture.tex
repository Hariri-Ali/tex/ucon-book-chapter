\section{Architecture}\label{sec:architecture}
%Architecture (2 pages) – Amjad 
%Architecture Components (PIP, PAP, PDP…) 
%Focus on new components such as PIP Registry, Obligation Manager (and internal obligations), Message Bus, ALFA Evaluator, Custom/Auxiliary Evaluators
%Workflow of Authorization Sessions
% connect to the model described and re-elicit requirements 
This section presents \ac{ucsp}, an architecture that realizes the \ac{ucon}+ model presented in the previous section. \ac{ucsp} is mainly based on the architecture put forward by the \ac{xacml}\cite{xacml} standard and by the \ac{ucon} framework introduced by Lazouski et. al ~\cite{ucs2012prototype} with several enhancements that we added. We start this section by eliciting the fundamental requirements to be filled by this architecture, and then we describe the components that build up this architecture. Lastly, we illustrate how these components interact to achieve the different required workflows.
\subsection{Requirements and Components Classification}
%requirements
The main objective of \ac{ucsp} is to enable controlling the usage of resources in a uniform way across the broad spectrum of the digital medium, e.g., cloud, edge, terminal, or \ac{iot}. The control functionality must accommodate to the mutability of contexts by providing functions to sense the environmental changes (e.g., by monitoring mutable attribute values). In turn, \ac{ucsp} must enable methods to grant and revoke decisions of access or usage. To achieve this ambitious goal in a modular, and extensible way, \ac{ucsp} comprises three categories of components that are: \emph{fixed}, \emph{programmable}, and \emph{configurable}. Firstly, fixed components are responsible for a specific task regardless of the environment or the application. Secondly, programmable components provide basic behavior and the skeleton of specific functionality. This behavior can be extended to realize any domain-specific requirements. Thirdly, configurable components have defined functionalities that do not change; however, they need to be adapted to cope with specific system properties. 

Regardless of their class, all the components communicate using a publish-subscribe model to manage the potential of increasing interactions efficiently. As such, each component registers to events and messages relevant to it. This enables a distributed infrastructure where a component can be spawned in several instances. Thus, \ac{ucsp} contains a \ac{msgbus} to handle our communication paradigm. In the following, we decompose the classes mentioned above into concrete components as illustrated by the \ref{fig:arch}.


\subsection{Components Description}
\begin{figure}[htb]%
	\centering
	\includegraphics[width=\textwidth]{figures/arch.png}
	\caption{The Architecture of \ac{ucsp} illustrating the different component.}
	\label{fig:arch}
\end{figure}
%\textcolor{red}{Ali: It seems we are using the terms Attribute Manager and Attribute Retreiver interchangeably. Maybe better to stick to one of them or highlight that they are the same thing.}

\ac{ch} is a \emph{fixed} core component that is responsible for receiving and routing access requests and managing the authorization workflow of such requests. The mentioned \ac{msgbus} is a sub-component that enables \ac{ch} to achieve this management of the workflow.

\ac{pap} is a \emph{configurable} component that acts as a data store of policies, policy sets and namespaces in the system. It offers \ac{api} to retrieve, add or update them. This component can be tuned according to different scenarios to persist policies in, e.g., SQL, NO-SQL, or in-memory databases.

\ac{sm} is a \emph{configurable} component that is responsible for the continuity of control. Essentially, it is a data structure that stores information about all active sessions. Depending on their authorization workflow, sessions transition among different phases, i.e., \emph{pre}, \emph{ongoing}, and \emph{post}. As such, for a specific session, the first entry in \ac{sm} is the event following the (Permit) response to the request. Semantically, this means that access has been granted, but the actual usage of the resource still did not start. Later, when the usage of the resource begins, \ac{sm} changes the status for this session, and \ac{ucsp} starts tracking any changes of the mutable attributes. Furthermore, \ac{sm} keeps a record of the relevant policy for a session, the analyzed request, and a pointer to the relevant attributes. In cases of access revocation, \ac{sm} updates the session status to Revoked. The session is kept in such a state until all possible obligations are handled, and the access is stopped. Afterwards, the session record is removed from the \ac{sm}.

\ac{pip} is a \emph{programmable} component that defines where to retrieve the values of specific attributes and how to monitor them. \ac{pip} is also used to trigger a change in the value of an attribute. A \ac{pip} implementation depends on the specific attribute retriever it interacts with; hence, a \ac{pip} will be implemented based on the application-specific requirements. To support \ac{pip}, we added an additional component, \ac{pip} Registry, to manage \acp{pip} and track which \acp{pip} are responsible for which attributes. \ac{ar} is another component that supports this process by enabling querying and updating attribute values.


%\textcolor{red}{Ali: Better to not mention TLEE explicitly or just give it as an example. The important thing is to highlight that UCS+ supports auxiliary evaluators (other than AbacEE), and TLEE is one example.}

\ac{pdp} is a \emph{fixed} component that is responsible for evaluating a request and based on a policy. The result of this evaluation can either be a \textit{PERMIT} (access is authorized), \textit{DENY} (access is denied), \textit{NOT APPLICABLE} (decision cannot be taken due to semantic reasons, e.g., no rules in the policy are matched), or \textit{INDETERMINATE} (decision cannot be taken because either the policy or the request is malformed). To form a decision, the \ac{pdp} parses the request, resolves the relevant attributes, and then matches them with the values in the policies conditions. To perform the evaluation, \ac{pdp} may leverage different expression evaluators. The core evaluator in UCS+ is AbacEE to resolver \ac{abac} expressions. UCS+ can also be extended with custom evaluators for particular expressions, such as the one contributed in~\cite{ucs2020plus} for \ac{tlee} expressions. To handle conflicts, i.e., a single request having several applicable rules, combining algorithms are used to determine the priorities among rules and resolve conflicts. 


\ac{om} is a \emph{programmable} component that handles a crucial part of the \ac{ucon}+ model, the obligations. Obligations indicate additional actions that must be performed together with access decision enforcement. Technically, obligations are abstract actions that are not bound to a specific format. For example, obligations can be used to control the values of attributes; in such cases, they are called attribute-update obligations.

\ac{at} is an auxiliary \emph{configurable} component that we added to manage attributes. It is especially needed in environments with faulty attribute retrievers where there is a possibility that values are not promptly available. While \ac{pip} collects the values of the attributes, the \ac{at} is responsible for caching these values to handle the periodic polling of values as part of the continuous attribute monitoring. In case the attribute retriever does not include any subscription mechanisms, which would allow the \ac{pip} to be notified when an attribute value changes, the viable strategy is to query the attribute retriever periodically. In such a case, \ac{at} registers the polling time needed for each attribute according to its criticality.

Lastly, the \ac{pep} is the component that integrates the \ac{ucsp} with the target application. It is responsible for intercepting usage and access requests within the application and directing them to the \ac{ucsp} by interacting with \ac{ch}. Then, it handles the results of these interactions, e.g., allowing the access, or performing an obligation.

To wrap up this section, we present a sequence diagram of the core workflow of \ac{ucsp}. Figure \ref{fig:seqArch} illustrates a simplified flow of how \ac{ucsp} evaluates.

\begin{figure}[!htb]%
	\centering
	\includegraphics[width=0.96\textwidth]{figures/arch2.pdf}
	\caption{A sequence diagram of the core \ac{ucsp} workflow.}
	\label{fig:seqArch}
\end{figure}