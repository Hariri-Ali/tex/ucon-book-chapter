\section{Introduction}\label{sec:introduction}
Access control has been used to protect data privacy and security.
Many access control models exist to address different security requirements.
They evolved from coarse-grained models such as \ac{mac}~\cite{sandhu1993lattice}, \ac{dac}~\cite{sandhu1998discretionary} and \ac{rbac}~\cite{sandhu1996role} to more flexible and fine-grained models such as \ac{abac}~\cite{abac}.
\ac{abac} provides higher level of granularity by managing access rights using policies that are based on attributes of subjects, resources and the environment.
It also incorporates obligations and advice, which are respectively mandatory and optional actions to be completed upon evaluation of policies.

Cyber-physical systems and consumer \ac{iot} devices, together with an ever-expanding network of sensors and actuators, help interconnect people, appliances as well as \ac{ict} resources pervading homes, vehicles, healthcare systems and many other aspects of human life.
The continuity of interaction combined with the heterogeneity of devices, information, connectivity and automation creates a  dynamic environment.
Monitoring and controlling permissions, access and usage of information and resources in such environments is essential for maintaining quality, security and safety.
However, existing \ac{iam} technologies and models (e.g., \ac{abac}) fail to offer an effective and cost-efficient solution because they assume that access rights do not change during access, so they do not react to situation changes.

For this reason, we introduce a comprehensive model and a novel technology for continuous authorization.
Our model builds on the \ac{ucon} model~\cite{ucon,ucs2012prototype}, which extends \ac{abac} with mutable attributes and continuous control.
We designate this technology as \ac{ucon}+ because it enhances \ac{ucon} with the following novelties:
\begin{enumerate*}[label={(\arabic*)}]
    \item Extend continuous monitoring to cover interactions before granting and after revoking authorizations;
    \item Enable involving auxiliary evaluators in the decision making process (e.g., trust level evaluation, threat intelligence);
    \item Support policy administration and delegation as well as defining trust authorities and roots of trust; and
    \item Leverage a lightweight policy language for \ac{abac} and extend it with \ac{ucon} novelties without modifying the syntax and semantics of the language.
\end{enumerate*}
We also describe \ac{ucsp}, a modular, scalable and lightweight implementation of \ac{ucon}+.
\ac{ucsp} is an robust dynamic authorization technology for embedded systems as well as cloud and \acp{zta}, benefiting them with:
\begin{itemize}
    \item Policy-based and code-less behavior at the ``brain'' of such systems;
    \item Continuous monitoring of context, environment and data/resource access;
    \item Intelligent (algorithmic) combination of policies with manageable resolution of conflicts;
    \item Proactive decisions and interactions to improve user experience, optimize usage of apps and resources, mitigate security and safety risks;
    \item Automated response to change including revocation of access or restriction of privileges; and
    \item Ability to advice or prompt for input and involve humans in real-time decision making.
\end{itemize}
This chapter consolidates our recent advancements on \ac{ucon}+ with our previous works presented in~\cite{siuv,datapal,ucs2020plus}.