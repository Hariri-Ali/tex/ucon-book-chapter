\section{The UCON+ Model}\label{sec:model}
The \ac{ucon} model was introduced as a generalization that goes beyond \ac{abac} by adding mutability of attributes and continuity of control.
\ac{ucon} was originally introduced by Park and Sandhu~\cite{ucon} focusing on the convergence of access control and \ac{drm}, and extending them with context-based conditions.
Unlike preceding models, \ac{ucon} considers attributes to be mutable, and specifies that policies must be re-evaluated when attribute values change during usage of resources.
Lazouski et. al~\cite{ucs2012prototype,uxacml} introduced a particularly relevant flavor of \ac{ucon} that preserves a full \ac{abac} baseline model, complemented with an authorization context, continuous monitoring and policy re-evaluation as well as an implicit temporal state.
The temporal state is captured by classifying policy rules as \emph{pre}, \emph{ongoing} and \emph{post}, such that \emph{pre} rules are evaluated before granting authorization, \emph{ongoing} rules are continuously evaluated while auth orization is in progress, and \emph{post} rules evaluated upon the end or revocation of authorization.
The novelties of \ac{ucon} enable it to monitor attribute values and re-evaluate policies upon changes in order to guarantee that access rights still hold whilst usage is in progress, or to revoke access if the security policy is no longer satisfied. 
This makes \ac{ucon} an excellent baseline for dynamic environments and cyber-physical systems where contextual changes are frequent and authorizations are long-lived.

Continuous control in \ac{ucon} is limited to the duration of active authorizations only (i.e., only when access has started and is in progress).
For this reason, we introduce \ac{ucon}+, an improvement of the \ac{ucon} model that extends continuous control to cover interactions before granting and after revoking authorization.
This allows \ac{ucon}+ to cover use-cases that require continuous control and monitoring before and after authorization (e.g., allow subject to improve trust level before granting authorization, ensure that a smart vehicle stops safely upon revoking authorization).
In addition, \ac{ucon}+ enhances \ac{ucon} with the ability to involve auxiliary decision factors such as trust level or threat intelligence.
\ac{ucon}+ also extends \ac{ucon} with the \ac{adp}~\cite{oasis2014admindel} by incorporating administrative policies that define trust authorities and allows their delegation.
In this section, we describe the \ac{ucon}+ model as well as the policy language used to express \ac{ucon}+ policies.

% ABAC’s evaluation semantics, architecture, administration and policy language are defined in a comprehensive standardised reference
% model known as the eXtensible Access Control Markup Language (XACML)
% [20]. 
% Applicable ABAC rules
% evaluate to an access control decision (typically PERMIT
% or DENY) while the evaluation process may also result in
% INDETERMINATE if some error occurs during the evaluation
% process (e.g., failure to obtain the value of some necessary
% attribute).

\subsection{Model Components}\label{sec:model-components}
The \ac{ucon}+ model consists of five main components adopted and adapted from \ac{ucon}~\cite{ucon,martinelli2016enforcement}.
The five components are: Attributes, Phases, Rules, Decisions and Instructions.
These components together compose authorizations as shown in Figure~\ref{fig:model} and described as follows.

\begin{figure}[htb]%
	\centering
	\includegraphics[width=\textwidth]{figures/model.pdf}
	\caption{\ac{ucon}+ Model Components.}
	\label{fig:model}
\end{figure}

%\subsubsection{Attributes}
\texttt{Attributes} are properties of the subject (e.g., name), resource (e.g., file type) or environment (e.g., CPU load).
Unlike \ac{ucon}, we consider attributes of all categories (i.e., subject, object and environment) to be mutable, so their values may change while an authorization is in progress.
An attribute value may change as a consequence of the authorization itself (e.g., updating metadata of a file upon granting authorization to access it) or due to contextual changes (e.g, subject location changes due to mobility).
Since attributes are mutable, the security policy must be re-evaluated whenever an attribute value changes throughout the lifetime of an authorization.

%\subsubsection{Phases}\label{phase}
\texttt{Phases} refer to the temporal state introduced by \ac{ucon} and describe the stages of an authorization starting from initial request to enforcement to revocation.
Like \ac{ucon}, \ac{ucon}+ defines three phases:
\begin{enumerate*}[label={(\arabic*)}]
    \item the \emph{pre} phase indicates that and authorization has been requested but has not been granted yet;
    \item the \emph{ongoing} phase denotes that the authorization has been granted and is in progress; and
    \item the \emph{post} phase refers to the last stage of the the authorization in which post-usage actions are enforced.
\end{enumerate*}
In each of these three phases, attribute values and the authorization context are continuously checked in order to promptly react to changes.
This enables continuous control in the \emph{pre} and \emph{post} phases in \ac{ucon}+, which was not enacted in \ac{ucon}.

%\subsubsection{Rules}
\texttt{Rules} are functional predicates over attributes that must be evaluated to determine whether an authorization can be granted based on the values of attributes.
Rules are classified by phases such that each class is evaluated only when the authorization enters the corresponding phase.
For instance, \emph{ongoing} rules must be evaluated only when the authorization has been granted and is in progress.
Rules must be re-evaluated whenever an attribute value changes and the authorization decision must be updated accordingly.

%\subsubsection{Decisions}
\texttt{Decisions} are the evaluation outcome of rules.
\ac{ucon}+ extends the classical two-valued decisions (i.e., \emph{Permit}, \emph{Deny}) with two additional values as follows:
\emph{Indeterminate} indicates a decision cannot made due to an error or a missing attribute; and
\emph{NotApplicable} denotes that the system could not find any rule that matches the authorization request.
The decision of an authorization may alter between these values as the context changes throughout the different phases of the authorization as mentioned above.

%\subsubsection{Instructions}
\texttt{Instructions} are mandatory or optional actions defined in security policies and enforced upon evaluation.
Instructions include subject obligations, attribute updates and system actions.
For instance, a security policy may specify that the metadata of a file must be updated (attribute update).
Another example is a security policy specifying that the subject must complete \ac{mfa} (subject obligation), and an email must be sent to the sysadmin when the authorization is granted (system action).


\subsection{Authorization Session}\label{sec:session}
In traditional access control (e.g., \ac{rbac}, \ac{abac}), authorizations are momentary such that only one authorization decision is made per request.
\ac{ucon} introduced the concept of continuous authorization that spans over a period of time, during which the security policy is re-evaluated and the authorization decision is updated accordingly.
We designate the context of a single continuous authorization as the \emph{Authorization Session}.
An authorization session refers to all events, policy evaluations and contextual information that belong to the lifetime of a single continuous authorization.
An authorization session in \ac{ucon}+ is initiated by and associated with a single \emph{authorization request}.
As aforementioned, \ac{ucon}+ enacts continuous control after the revocation or end of authorization.
Thus, an authorization session may last beyond the revocation of authorization to ensure that post-usage actions are completed.
Therefore, a \ac{ucon}+ authorization session starts with a request and ends upon the enforcement of all \emph{post} rules.
Attributes that are relevant to a particular authorization session (i.e., required for policy evaluation) are continuously monitored throughout the session, and a re-evaluation of the security policy is triggered whenever an attribute value changes.

\subsection{Policy Language}\label{sec:policylang}
 \ac{xacml} is the OASIS standard policy language to express \ac{abac} policies~\cite{xacml}.
 \ac{xacml} cannot be used to express \ac{ucon} policies as it does not include phases, which are a main component of \ac{ucon} and \ac{ucon}+ as mentioned in Section~\ref{sec:model-components}.
 To enable the novelties of \ac{ucon}, Colombo et al. introduced U-\ac{xacml}~\cite{uxacml,uxacmlgit}, an extension of \ac{xacml} that incorporates \ac{ucon} concepts.
 However, U-\ac{xacml} is incompatible with \ac{xacml} as it includes some modifications of the standard.
 For example, U-\ac{xacml} rules consist of multiple conditions to be evaluated during \emph{pre}, \emph{ongoing} and \emph{post} phases.
 This change is not compatible with the \ac{xacml}, which allows only one condition per rule.
 Thus, U-\ac{xacml} requires specific modifications to evaluators making its adoption harder.
 Moreover, \ac{xacml} and U-\ac{xacml} are verbose and complex languages, which undermines their readability and efficiency.
 
 To overcome the above drawbacks, \ac{ucon}+ uses \ac{alfa}~\cite{alfa}, a pseudocode domain-specific policy language that is in the standardization process by OASIS.
 \ac{alfa} maps directly to \ac{xacml} without adding any new syntax or semantics, and is used to express \ac{abac} policies.
 Instead of modifying the \ac{alfa} standard, we express \ac{ucon}+ phases as attributes specified during a usage request, unlike U-\ac{xacml} which defines phases as conditions within a single rule.
 \ac{alfa} is much less verbose than \ac{xacml}, which makes it more human readable and shorter in size allowing faster parsing and evaluation.
 \ac{alfa} adheres to the same hierarchy as \ac{xacml} where decision predicates are expressed in rules that are nested under policies, which in turn are nested under policy sets and/or namespaces.
 Policies and rules resolve to one of the decisions described in Section~\ref{sec:model-components} (i.e., \emph{Permit}, \emph{Deny}, \emph{Not Application}, \emph{Indeterminate}) based on their evaluation by the \ac{alfa} policy engine.
 Like \ac{xacml}, \ac{alfa} relies on combining algorithms to resolve conflicts between sibling rules or policies.
 \ac{alfa} also allows the use of functions, such as regular expression, string concatenation and others, which helps to further refine applicability of policies and rules.
 Listing~\ref{lst:alfa} gives an example \ac{alfa} policy that limits the action of opening the front door of the house to its owner.
 The policy adheres to a non-inclusive hierarchy expressed in~\ac{alfa} syntax as a nested structure of graph parentheses.
 Thus, each \ac{alfa} element is enclosed between parentheses and nested under the parent element. 

The ``target'' clause at line 2 determines the applicability of the policy ``door'' based on the values of the \texttt{resource} and \texttt{action} attributes, which must be \texttt{FRONT\_DOOR} and \texttt{OPEN} respectively.
Line 4 specifies the combining algorithm \emph{firstApplicable} which takes the result of the first applicable rule, in this case the result of \emph{permitIfOwner} defined at line 5.
There is only one rule \emph{permitIfOwner} and it is applicable if its \emph{target} matches the value \texttt{employee}.
If applicable, the decision of this rule evaluates to \emph{permit} and includes an optional instructions (i.e., advice) as expressed in lines 9-14.
The instruction assigns the value \texttt{open}, \texttt{door} and \texttt{email} to attributes \texttt{command}, \texttt{resource} and \texttt{channel} respectively.
\ac{ucon}+ also supports administration and delegation \ac{alfa} policies, which allow resource owners and administrators to determine who is allowed to issue policies about their resources.
This is further described in the following section.

\begin{lstlisting}[caption = An example \ac{alfa} policy, label=lst:alfa]
policy door {
    target clause Attributes.resource == FRONT_DOOR && 
                    Attributes.action == OPEN
    apply firstApplicable
    rule permitIfOwner {
        target clause Attributes.role == "employee"
        permit
        on permit {
            advice notify {
                command = open
                resource = door 
                channel = email
            }
        }
    }
}
\end{lstlisting}

\subsection{Administration and Delegation}\label{sec:delegation}
% \textcolor{red}{Ali: This seems to be an exact copy from DataPAL. Fabio mentioned that we cannot re-use text exactly as it is from other papers, so please rephrase}

% \textcolor{red}{Ali: We can summarise the text here as it is just describing XACML's ADP. What we need to highlight is our contribution. This is sections IV-B and IV-C from DataPAL. Maybe also add a brief example from Section IV-D in DataPAL.}

% \textcolor{blue}{Subhajit : I'm travelling and haven't got the time yet to write yet. This text I had put in just as a placeholder.}



Policy administration controls the types of policies that individuals can create and modify for specific groups of resources.
Different classifications such as action type, topic, or some other property are also possible under which policy issuance, revocation or any change can take place.
Further operations like delegation are also possible within policy administration, where delegation of authority, or the delegation of the right to issue/revoke policies are possible.
%\ac{adp} \cite{oasis2014admindel} is a profile introduced in \ac{xacml} 3.0 to address use-cases in which administration of policy issuance or delegation of policy authority is required.
For instance, system administrators or resource owners may want to restrict and control who can write policies about their resources.
As another example, a resource owner may want to delegate their authority to someone else based on some conditional statements, which could be absence from office or conditions based on time, location etc.
The \ac{adp} \cite{oasis2014admindel} supports such use cases and allows administrators to write policies about other policies forming trees that start with top level policies designated as root of trust. 
%In the scope of this paper, we refer to controller policies (i.e. policies that control other policies) as \textit{Administrative Policies}.
%On the other hand, we designate controlled policies (i.e. access policies) as \textit{Usage Policies}.
%As such, administrative policies define who is authorized to write other administrative policies or specific usage on data, resources or services.

% The \ac{adp} mentions that a usage policy will not be enforced unless it is explicitly authorized by administrative policies.
%Thus, a Usage Policy can be \textit{Admissible} if its enforcement is authorized by all upper layer policies, or \textit{NotAdmissible} if one of the Administrative Policies in the hierarchy cannot be considered.
The truth table mentioned in Table \ref{tab:evaluation outcomes} illustrates the outcomes given the \emph{Admissible} and \emph{NotAdmissible} evaluation effects combined with the Usage policy evaluation effects.
% which shows that whenever Administrative Policies are admissible, a final result is obtained.
% On the other hand, if one of the Administrative Policies is NotAdmissible, then another policy needs to be found.
\begin{table}[!htbp]
    \centering
    \caption {Evaluation Outcomes} \label{tab:evaluation outcomes}
    \resizebox{0.70\linewidth}{!}{
    \begin{tabular}{|c|c|c|}
            \hline
            \textbf{Usage Policy}  & \textbf{Administrative Policy} & \textbf{Result} \\
            \hline
            Permit & Admissible & Permit \\
            \hline
            Deny & Admissible & Deny \\
            \hline
            Permit & NotAdmissible & Find other policy \\
            \hline
            Deny & NotAdmissible & Find other policy \\
            \hline
        \end{tabular}
    }
\end{table}
Indeed, in some cases, multiple policies may be Admissible and have a Permit/Deny result.
For this reason, a policy reduction algorithm is used, which we discuss in detail in Section \ref{sec:reduction}.
% It is of vital importance to manage inconsistencies and to guide editing of administrative policies because otherwise all evaluations will end up in a NotAdmissible state. 
% To achieve this, once an administrative policy is added, compliance with the above layers can be checked and suggestions for improvements may be provided.
Editing a full-fledged policy with \ac{adp} incorporates the steps as mentioned in Listing \ref{lst:policyediting}.
\begin{lstlisting}[float=!htb, caption={Evaluation and Reduction process},label={lst:policyediting}, escapeinside={(*}{*)}]
<given a root policy defined by authority / organization>
user writes custom policy Pa
synthetic request is crafted from Pa
request is evaluated against Pa
    select administrative policy Padmin for Pa
        for each Pi in Padmin
            create administrative request Ra using R and Pi
            evaluate Ra against Pi
            if deny
                <show modifications>
            else
                <allow policy>
\end{lstlisting}

In our previous work \cite{datapal}, we introduced the ``\texttt{PolicyIssuer}'' keyword to \ac{alfa}, which identifies the author of the policy, and is used to create an administrative request.
This enables the use of \ac{alfa} to express and issue administrative policies and to support \ac{adp} in \ac{ucon}+.
% In addition, we reserved the following attribute categories to be used for \ac{adp}:
% \begin{itemize}
%     \item \texttt{Attributes.delegate} attribute category used in administrative requests to carry the attributes of the issuer of the policy which is being reduced.
%     \item \texttt{Attributes.delegation-info} attribute category used in administrative requests to carry information about the reduction in progress, such as the decision being reduced.
%     \item \texttt{Attributes.delegated} category used to carry information about the situation which is being reduced.
% \end{itemize}
% These extensions support the policy model and policy editing workflow described in the following subsection.

% \begin{lstlisting}[float=!htb, caption={Structure of an \ac{alfa} policy},label={lst:policy_structure}]
% policy <Title> {
%   target clause
%             Attributes.<AttributeName> == <AttributeValue>
%   apply <CombiningAlgorithm>
%   rule <RuleName> {
%     target clause
%             Attributes.<AttributeName> == <AttributeValue>
%     condition <ConditionExpression>
%     <decision (permit or deny)>
%     on <decision> {
%       <instruction (obligation)> <ObligationName> { 
%         <Attribute> = <Value>
% }}}}
% \end{lstlisting}

% \subsection{Policy Editing and Evaluation Workflow}\label{sub:polediting}
% Using the extensions we added to \ac{alfa}, we introduce the following policy editing workflow, which is also demonstrated in the example workflow shown in Figure \ref{fig:policyediting}.
% The \textit{\ac{ciso}} is in charge of creating top-level root-of-trust administrative policies that scope organisational, regulatory and business constraints.
% For instance, the \ac{ciso} specifies the actions, resources, usage purposes and other attributes that \ac{ds} and other actors are allowed to control in their policies.
%Managers can also add another level of administration by adding administrative policies that scope departmental regulations and constraints.
% \textit{\ac{ds}} (i.e. data owner) is delegated by the \ac{ciso}, and is in charge of creating administrative policies that delegate authority to other actors (e.g. managers).
% \ac{ds} policies define who can write usage policies about their data, and what kind of actions and data processing operations can these usage policies control.
% Finally, managers write usage policies about the data and define the constraints for data usage and processing. 
% In summary, the \ac{ciso} group delegates to the \ac{ds} group, and the \ac{ds} group in turn delegates to the managers group, which permits or denies staff from accessing data.
% An example of an administrative and usage policies is shown in Listing \ref{lst:admin_policy}, where the \ac{ciso} policy allows all data subjects to write policies about any action performed on their data.
% Similarly, the data subject policy allows any manager to write policies about CRUD operations performed on the data. 
% Finally, the staff policy is a usage policy that allows staff member Bob to read the data.


% \begin{figure}[!htb]
%     \centering
%     \includegraphics[scale=0.45]{images/DataSubject.png}
%     \caption{Policy editing workflow}
%     \label{fig:policyediting}
%     \centering
% \end{figure}


The policy evaluation flow can be outlined in two steps: reduction and combination.
\emph{Reduction} determines whether a usage policy was written by an authorized personnel and whether the evaluation outcome can be considered or not.
This is achieved by creating an evaluation tree that has a root-of-trust policy as its root node and the results of applicable policies as its edges, and then finding the branches that can reach the root-of-trust.
In the \emph{combination} step, the \ac{pdp} combines all valid results using combining algorithms as specified in the policies.
% This evaluation flow is described in Listing \ref{lst:evaluationReduction}. 
The \ac{adp} defines a maximum delegation depth to limit the number of administrative policies to be evaluated.
Accordingly, the path is discarded if the number of nodes in a path in the reduction graph exceeds the maximum delegation depth.
% The maximum delegation depth is not supported in this version of our framework.
% However, it will be added in future iterations.



% \begin{lstlisting}[float=!htb, caption={Example of administrative policies},label={lst:admin_policy}]
% policyset l3_ad_policy_set {
%   apply denyUnlessPermit
%   policy ciso_retain {
%     target clause
%       regex(Attributes.delegated.action, "\/action\/(.*)") &&
%       regex(Attributes.delegate.subject, "\/dataSubjects\/(.*)")
%     apply denyUnlessPermit
%     rule r1 {
%       permit
%     }
%   }
%   policy dataSubject_retain {
%     policyIssuer { Attributes.subject = "/dataSubjects/Carol" }
%     apply denyUnlessPermit
%     target clause
%       regex(Attributes.delegated.action, "\/action\/datalifecycle\/(.*)")
%         &&
%       regex(Attributes.delegate.subject, "\/employees\/manager\/(.*)")
%     rule r2 {
%       permit
%     }
%   }
%   policy staff_retain {
%     policyIssuer {Attributes.subject = "/employees/manager/Joe"}
%     apply denyUnlessPermit
%     target clause
%       Attributes.subject == "/employees/staff/Bob" &&
%       Attributes.action == "/action/datalifecycle/retain"
%       ...
%   }
% }
% \end{lstlisting}

For both administrative and usage policies, we use the \emph{denyUnlessPermit} combining algorithm due to its deterministic and restrictive nature.
This algorithm is restrictive because its default result is always \emph{deny}, unless there is an explicitly applicable permit rule.
This also eliminates any indeterministic results like \emph{indeterminate} when an attribute value is missing or a condition is false.
Therefore, when an administrative policy evaluates into \emph{indeterminate}, the corresponding branch is discarded from the entire delegation tree.
% Nevertheless, we intend to analyze the use of different combination algorithms and their impact and risks on the delegation tree and authorization decisions.


% \begin{lstlisting}[float=!htb, caption={Evaluation and Reduction process},label={lst:evaluationReduction}, escapeinside={(*}{*)}]
% <given a policy set PS and a request R>
% evaluate R against PS
% find applicable policies Papp
% for each applicable policy Pa in Papp
%     if deny
%         continue
%     if PolicyIssuer is absent 
%         then combine
%     else 
%         [selection step] : select administrative policy Padmin for Pa
%             for each Pi in Padmin
%                 create administrative request Ra using R and Pi
%                 evaluate Ra against Pi
%                 if deny
%                     no edge
%                 else
%                     add edge
%                     if !policyIssuer 
%                         potential path is found
%                     else 
%                         go to selection step with Pi as Pa

% combine edge results with PS combining algorithm
% \end{lstlisting}



% \subsection{Data Protection and Retention Policies}
% The above model has enabled a new approach to data privacy. 
% This is because CISOs can write administrative policies that state the characteristics of delegated actors, such as data owners, and the aspects on which these actors can write policies. 
% Undoubtedly, administrative policies \textit{must} list only allowed operations in order to protect user privacy.
% Furthermore, data owners can state possible data manipulation or anonymization techniques to be used whenever their data are used, adding an extra level of protection to better guarantee user privacy.
% Since administrative policies can be stored in remote or separate storage or systems, e.g., individual wallets, data owners have the capability of modifying them as they wish. 
% Data owners can eventually further delegate some aspects, if they want to, to data managers.
%Having administrative policies under their control enables data owners to better monitor their data usage and authorizations. 

% Data lifecycle policies are, afterall, usage control policies, and as such, their rules can be classified by the \ac{ucsp} phases (i.e. ``\textit{pre}'', ``\textit{ongoing}'' and ``\textit{post}'').
% Modeling data lifecycle policies according to \ac{ucsp} phases allows data owners to define usage policies related to different stages of a usage session, giving them full control over the lifecycle of their data, from collection to treatment to destruction (Figure \ref{fig:datalifecycle}).
% For instance, \ac{ucsp} phases can be used to control the data lifecycle as follows:
% \begin{itemize}
%     \item \textit{pre}: The data owner needs to provide consent for new data before it is collected and accessed for the first time. First-time collection and access consent may be recurrent over a time period.
%     \item \textit{ongoing}: Once data is collected and first-time accessed; consent defines the conditions of data usage, storage, transfer, and etc.
%     \item \textit{post}: Finally, once data is no longer used or access/usage is violated, consent defines the conditions of archival, deletion, destruction, and etc. of the data as well as any associated clean-up tasks. 
% \end{itemize}
% An example of such policy is shown in Listing \ref{lst:data_policy}.
% This makes the system GDPR-compliant and reduces the need to trust and rely on third parties for proper data management and security.
% EU regulations, such as \ac{eidas} \cite{eu-910-2014}, go exactly in this direction, that is guaranteeing data owners full control on their data and to, eventually, explicitly and dynamically authorize each and every attempt to use their data \cite{gdpr}. 
% By adding an additional layer of administrative delegation policy, one can also take into account regulations and norms stated by EU or other authorities \cite{gdpr}.
% This is because it is possible to have a first layer where regulation is defined, another where each user states their own administrative policy and finally usage policies are defined.

% \begin{lstlisting}[float=!htb, caption={Example of data lifecycle policy},label={lst:data_policy}]
% policy staff_retain {
%     policyIssuer {Attributes.subject = "/employees/manager/Joe"}
%     target clause
%       Attributes.subject == "/employees/staff/Bob" &&
%       Attributes.action == "/action/datalifecycle/retain" &&
%       Attributes.dataRecord.id == "entryData" &&
%       Attributes.dataRecord.owner == "dataSubject" &&
%     Attributes.purpose == "userExperienceOptimization"
%   apply denyUnlessPermit
%   rule pre {
%     target clause Attributes.ucs.step.pre
%     condition Attributes.dataRecord.userConsent
%     permit
%     on permit {
%       obligation create {
%         dataRecord = Attributes.dataRecord.id
%       }
%     }
%   }
%   rule ongoing {
%     target clause Attributes.ucs.step.ongoing
%     condition
%       Attributes.dataRecord.userConsent and
%       Attributes.dataRecord.encrypted and
%       Attributes.dataRecord.expiryDate > Attributes.currentTime
%     permit
%   }
%   rule post {
%     target clause Attributes.ucs.step.post
%     deny
%     on deny {
%       obligation delete {
%         msg = "Record deleted"
%         dataRecord = Attributes.dataRecord.id
%       }
%       obligation notify {
%         dataRecord = Attributes.dataRecord.id
%         hasUserConsented = Attributes.dataRecord.userConsent
%         isEncrypted = Attributes.dataRecord.encrypted
%         isDataExpired =
%           (Attributes.dataRecord.expiryDate < Attributes.currentTime)
%       }
%     }
%   }
% }
% \end{lstlisting}
